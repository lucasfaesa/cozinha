﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateTexture : MonoBehaviour
{
    [SerializeField]
    private List<GameObject> movelObject;

    [SerializeField]
    private Texture2D movelTexture;

    [SerializeField]
    private ChangeTexture changeTexture;

    private Texture2D rotTexture;

    private Texture2D previousTexture;

    private void Start()
    {
        movelObject.Add(GameObject.Find("Bancada").gameObject);
        movelObject.Add(GameObject.Find("Pia").gameObject);
        changeTexture = GetComponentInParent<ChangeTexture>();
    }
    public void TextureToRotate()
    {
        if(movelObject[0].GetComponent<Renderer>().material.name != changeTexture.materialMoveis.name + " (Instance)" || movelObject[0].GetComponent<Renderer>().material.mainTexture != previousTexture)
        {
            changeTexture.ChangeTextureOnClick();
        }

        for (int i=0; i < movelObject.Count; i++)
        {
            movelTexture = movelObject[i].GetComponent<Renderer>().material.mainTexture as Texture2D;

            movelObject[i].GetComponent<Renderer>().material.mainTexture = rotateTexture(movelTexture, true);
        }
    }

    Texture2D rotateTexture(Texture2D originalTexture, bool clockwise)
    {
        if(rotTexture != null)
        {
            previousTexture = rotTexture;
        }

        Color32[] original = originalTexture.GetPixels32();
        Color32[] rotated = new Color32[original.Length];

        int w = originalTexture.width;
        int h = originalTexture.height;

        int iRotated, iOriginal;

        for (int j = 0; j < h; ++j)
        {
            for (int i = 0; i < w; ++i)
            {
                iRotated = (i + 1) * h - j - 1;
                iOriginal = clockwise ? original.Length - 1 - (j * w + i) : j * w + i;
                rotated[iRotated] = original[iOriginal];
            }
        }

        rotTexture = new Texture2D(w, h, TextureFormat.RGB24, false);
        rotTexture.SetPixels32(rotated);
        rotTexture.Apply();
        return rotTexture;
    }
}
