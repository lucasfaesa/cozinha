﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextureButtonsController : MonoBehaviour
{
    [SerializeField]
    private GameObject textureButton;
    
    [SerializeField]
    private bool show;

    // Start is called before the first frame update
    void Start()
    {
        textureButton.GetComponent<Animator>().Play("OutState");
        show = false;
        //textureButton.SetActive(false);
    }

    public void ShowTextureButton()
    {
        HideButtons();

       // textureButton.GetComponent<Animator>().Play("InState");

        show = !show;
        
        textureButton.SetActive(show);

    }

    private void HideButtons()
    {
        textureButton.GetComponent<Animator>().Play("InState");

        TextureButtonsController[] textureBT = FindObjectsOfType<TextureButtonsController>();
        
        foreach (TextureButtonsController txtBT in textureBT)
        {
            if(txtBT != this)
            {
                txtBT.show = false;
                txtBT.textureButton.SetActive(false);
            }
        }

    }
}
