﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextureHolder : MonoBehaviour
{
    [Header("Texturas Bancada")]
    public Material[] materiaisBancada;
    public Texture2D[] texturasBancada;
    public Sprite[] thumbnailsBancada;

    [Header("Texturas Parede")]
    public Texture2D[] texturasParede;
    public Sprite[] thumbnailsParede;

    [Header("Texturas Piso")]
    public Texture2D[] texturasPiso;
    public Sprite[] thumbnailsPiso;

}
