﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField]
    private float playerSpeed = 5f;
    [SerializeField]
    private float verticalSpeed = 3f;
    [SerializeField]
    private CharacterController controller;

    // Update is called once per frame
    void Update()
    {
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        Vector3 moveHorizontal = transform.right * x + transform.forward * z;
        Vector3 moveVertical = transform.up * verticalSpeed;

        controller.Move(moveHorizontal * playerSpeed * Time.deltaTime);

        if (Input.GetKey(KeyCode.E))
        {
            controller.Move(moveVertical * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.Q))
        {
            controller.Move(-moveVertical * Time.deltaTime);
        }
        
    }
}
