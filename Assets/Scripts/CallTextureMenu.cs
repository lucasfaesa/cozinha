﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CallTextureMenu : MonoBehaviour
{

    [SerializeField]
    private bool menuIn;
    [SerializeField]
    private Animator textureMenuAnimator;
    // Start is called before the first frame update


    public void CallMenu()
    {
        menuIn = !menuIn;

        if (menuIn){
            textureMenuAnimator.Play("MenuIN");
        }
        else
        {
            textureMenuAnimator.Play("MenuOUT");
        }
    }
}
