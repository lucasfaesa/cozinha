﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeTexture : MonoBehaviour
{
    [SerializeField]
    private Texture2D textureMoveis;


    public Material materialMoveis;

    [SerializeField]
    private List<Renderer> movelMesh = new List<Renderer>();

    [SerializeField]
    public enum movel { bancada, parede, piso};

    public movel movelParaTexturizar;

    // Start is called before the first frame update
    void Start()
    {
        if (movelParaTexturizar == movel.bancada)
        {
            movelMesh.Add(GameObject.Find("Bancada").GetComponent<Renderer>());
            movelMesh.Add(GameObject.Find("Pia").GetComponent<Renderer>());
        }
        if(movelParaTexturizar == movel.parede)
        {
            movelMesh.Add(GameObject.Find("ParedeJanela").GetComponent<MeshRenderer>());
            movelMesh.Add(GameObject.Find("ParedePia").GetComponent<MeshRenderer>());
            movelMesh.Add(GameObject.Find("ParedePorta").GetComponent<MeshRenderer>());
            movelMesh.Add(GameObject.Find("ParedeTras").GetComponent<MeshRenderer>());
        }
        if(movelParaTexturizar == movel.piso)
        {
            movelMesh.Add(GameObject.Find("Piso").GetComponent<MeshRenderer>());
        }
    }

    public void ChangeTextureOnClick()
    {
        if(movelParaTexturizar == movel.bancada)
        {
            for (int i = 0; i < movelMesh.Count; i++)
            {
                movelMesh[i].material = materialMoveis;
            }
        }
        else
        {
            for(int i=0; i< movelMesh.Count; i++)
            {
                 movelMesh[i].material.mainTexture = textureMoveis;
            }
        }
    }

    public void TextureHolder(Texture2D texture)
    {
        textureMoveis = texture;
    }

    public void MaterialHolder (Material material)
    {
        materialMoveis = material;
    }

}
