﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeQuality : MonoBehaviour
{
    private enum quality { low, medium, high};

    [SerializeField]
    private quality qualitySelector;

    public void ChangeRuntimeQuality()
    {
        Quality(qualitySelector);
    }

    private void Quality(quality number)
    {
        switch (number)
        {
            case quality.low:
                QualitySettings.SetQualityLevel(0, true);
                Debug.Log("low");
                break;

            case quality.medium:
                QualitySettings.SetQualityLevel(3, true);
                Debug.Log("medium");
                break;

            case quality.high:
                QualitySettings.SetQualityLevel(5, true);
                Debug.Log("high");
                break;
        }
    }
}
