﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using Newtonsoft.Json;
using Application = UnityEngine.Application;

public class TexturesInstantiator : MonoBehaviour
{
    [SerializeField]
    private GameObject buttonPrefab;

    [SerializeField]
    private Material placeholderMaterial;

    [SerializeField]
    private Texture2D[] texturesArray;

    private Texture2D textTemp;

    private int cont = 0;

    private List<ImageFiles> listaArquivos;

    void Start()
    {
    #if UNITY_EDITOR
        string m_Path = Application.streamingAssetsPath + "/Uploads/manifest.json";
    #else
        string m_Path = Application.dataPath + "/Uploads/manifest.json";
    #endif

        StartCoroutine(GetManifest(m_Path));
    }
    
    private IEnumerator GetManifest(string url)
    {
        string result = "";

        using (UnityWebRequest www = UnityWebRequest.Get(url))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.LogError("ERRO!!");
            }
            else
            {
                try
                {
                    result = www.downloadHandler.text;

                    Debug.Log(result);

                    listaArquivos = JsonConvert.DeserializeObject<List<ImageFiles>>(result);
                    listaArquivos = listaArquivos.OrderBy(x => x.name).ToList();

                    foreach (ImageFiles imgfiles in listaArquivos)
                    {
                     #if UNITY_EDITOR
                        StartCoroutine(GetTexture(Application.streamingAssetsPath+ "/Uploads/" + imgfiles.file, imgfiles.name));
                    #else
                        StartCoroutine(GetTexture(Application.dataPath + "/Uploads/" + imgfiles.file, imgfiles.name));
                    #endif          
                    }

                }
                catch (Exception x)
                {
                    Debug.Log("ERRO: " + x);
                }
            }
        }
    }

    private IEnumerator GetTexture(string url, string filename)
    {
        using (UnityWebRequest request = UnityWebRequestTexture.GetTexture(url))
        {
            yield return request.SendWebRequest();

            if (request.isNetworkError || request.isHttpError)
            {
                Debug.LogError("ERRO!!" + request.isHttpError);
            }
            else
            {
                try
                {
                    textTemp = new Texture2D(1024, 1024, TextureFormat.DXT1, false);
                    textTemp.LoadImage(request.downloadHandler.data);
                    InstantiateNewButton(textTemp, filename);
                    cont++;
                }
                catch (Exception x)
                {
                    Debug.Log("ERRO: " + x);
                }
            }
        }
    }

    private void InstantiateNewButton(Texture2D texture, string filename)
    {
        GameObject buttonInstantiated = Instantiate(buttonPrefab, transform.parent);
        Material materialInstantiated = Instantiate(placeholderMaterial);
        Sprite thumbnail = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0f, 0f), 1);

        buttonInstantiated.GetComponent<Image>().sprite = thumbnail;
        buttonInstantiated.GetComponentInChildren<Text>().text = filename;
        materialInstantiated.mainTexture = texture;
        buttonInstantiated.GetComponent<ChangeTexture>().MaterialHolder(materialInstantiated);    
    }
}
 