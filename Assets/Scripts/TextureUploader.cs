﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;
using UnityEngine.UI;
using UnityEngine.Networking;

public class TextureUploader : MonoBehaviour
{
    [SerializeField]
    private GameObject buttonPrefab;

    [SerializeField]
    private Material placeholderMaterial;

    private int cont = 0;
    [DllImport("__Internal")]
    private static extern void ImageUploaderCaptureClick();

    IEnumerator LoadTexture(string url)
    {
         using(UnityWebRequest www = UnityWebRequestTexture.GetTexture(url)){
            yield return www.SendWebRequest();
            if(www.isNetworkError || www.isHttpError) {
                Debug.Log(www.error);
            }
            else { 


                InstantiateButtonWithUploadedTexture(DownloadHandlerTexture.GetContent(www));
            }
        }
      /*  WWW image = new WWW(url);
        yield return image;
        Texture2D texture = new Texture2D(1, 1);
        image.LoadImageIntoTexture(texture);
        InstantiateButtonWithUploadedTexture(texture);
        Debug.Log("Loaded image size: " + texture.width + "x" + texture.height);*/
    }

    void FileSelected(string url)
    {
        StartCoroutine(LoadTexture(url));
    }

    public void OnButtonPointerDown()
    {
    #if UNITY_EDITOR
        string path = UnityEditor.EditorUtility.OpenFilePanel("Open image", "", "jpg,png,bmp");
        if (!System.String.IsNullOrEmpty(path))
            FileSelected("file:///" + path);
    #else
        ImageUploaderCaptureClick();
    #endif
    }

    private void InstantiateButtonWithUploadedTexture(Texture2D texture)
    {
        cont++;
        GameObject buttonInstantiated = Instantiate(buttonPrefab, transform.parent);
        Material materialInstantiated = Instantiate(placeholderMaterial);
        Sprite thumbnail = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0f, 0f),1);
        
        buttonInstantiated.GetComponent<Image>().sprite = thumbnail;
        buttonInstantiated.GetComponentInChildren<Text>().text = "Uploaded " + cont.ToString();
        materialInstantiated.mainTexture = texture;
        buttonInstantiated.GetComponent<ChangeTexture>().MaterialHolder(materialInstantiated);
    }
}
