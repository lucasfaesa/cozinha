﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonsGenerator : MonoBehaviour
{
    [SerializeField]
    private TextureHolder textureHolder;

    [SerializeField]
    private Scrollbar scrollBar;

    [SerializeField]
    private GameObject buttonPrefab;

    public enum movel { bancada, parede, piso };

    public movel texturaMovel;
    // Start is called before the first frame update
    void Awake()
    {
        textureHolder = FindObjectOfType<TextureHolder>();

        if(texturaMovel == movel.bancada)
        {
            Material[] materiaisBancada = textureHolder.materiaisBancada;
            Sprite[] thumbanailsBancada = textureHolder.thumbnailsBancada;

            for (int i =0; i< materiaisBancada.Length; i++)
            {
                GameObject buttonInstantiated = Instantiate(buttonPrefab, transform);

                buttonInstantiated.GetComponent<Image>().sprite = thumbanailsBancada[i];
                buttonInstantiated.GetComponentInChildren<Text>().text = thumbanailsBancada[i].name;
                buttonInstantiated.GetComponent<ChangeTexture>().MaterialHolder(materiaisBancada[i]);
            }
        }

        if(texturaMovel == movel.parede)
        {
            Texture2D[] texturasParede = textureHolder.texturasParede;
            Sprite[] thumbanailsParede = textureHolder.thumbnailsParede;

            for (int i = 0; i < texturasParede.Length; i++)
            {
                GameObject buttonInstantiated = Instantiate(buttonPrefab, transform);

                buttonInstantiated.GetComponent<Image>().sprite = thumbanailsParede[i];
                buttonInstantiated.GetComponentInChildren<Text>().text = thumbanailsParede[i].name;
                buttonInstantiated.GetComponent<ChangeTexture>().TextureHolder(texturasParede[i]);
            }
        }

        if(texturaMovel == movel.piso)
        {
            Texture2D[] texturasPiso = textureHolder.texturasPiso;
            Sprite[] thumbanailsPiso = textureHolder.thumbnailsPiso;

            for (int i = 0; i < texturasPiso.Length; i++)
            {
                GameObject buttonInstantiated = Instantiate(buttonPrefab, transform);

                buttonInstantiated.GetComponent<Image>().sprite = thumbanailsPiso[i];
                buttonInstantiated.GetComponentInChildren<Text>().text = thumbanailsPiso[i].name;
                buttonInstantiated.GetComponent<ChangeTexture>().TextureHolder(texturasPiso[i]);
            }
        }
    }

    private void Start()
    {
        StartCoroutine(ResetScrollBarValue());
    }

    private IEnumerator ResetScrollBarValue()
    {
        yield return new WaitForSeconds(1.5f);
        scrollBar.value = 1f;
    }

}
